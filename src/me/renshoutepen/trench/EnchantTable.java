package me.renshoutepen.trench;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.inventory.meta.ItemMeta;

public class EnchantTable implements Listener {

	@EventHandler
	public void onEnchant(EnchantItemEvent event) {
		Material mat = event.getItem().getType();
		if (mat.toString().contains("AXE")) {
			int trench = isTrench();
			if (trench > 0) {
				String lore = t("&7Trench " + convertRoman(trench));
				List<String> lores = new ArrayList<String>();
				lores.add(lore);
				ItemMeta meta = event.getItem().getItemMeta();
				meta.setLore(lores);
				event.getItem().setItemMeta(meta);
			}
		}
	}

	public String convertRoman(Integer i) {
		String s = "";
		for (int r = 0; r < i; r++) {
			s += "I";
		}
		return s;
	}

	public String t(String i) {
		return ChatColor.translateAlternateColorCodes('&', i);
	}

	public int isTrench() {
		Random random = new Random();
		int r = random.nextInt(100);
		if (r >= 70) {
			return 1;
		} else if (r <= 70 && r >= 60) {
			return 2;
		} else if (r <= 60 && r >= 55) {
			return 3;
		}
		return 0;
	}

}
