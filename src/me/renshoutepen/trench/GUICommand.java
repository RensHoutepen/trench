package me.renshoutepen.trench;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class GUICommand implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
		Player p = (Player) sender;
		if ((sender instanceof Player)) {

			Inventory trench = Bukkit.getServer().createInventory(p, 9, "Select a Trench level");

			// level 1

			ItemStack i1 = new ItemStack(org.bukkit.Material.ENCHANTED_BOOK);
			ItemMeta m1 = i1.getItemMeta();
			ArrayList<String> lore = new ArrayList<String>();

			lore.add("Digs a region of 3x3");

			m1.setLore(lore);
			m1.setDisplayName(ChatColor.BOLD + "Trench I");

			i1.setItemMeta(m1);
			trench.setItem(2, i1);

			// level 2

			ItemStack i2 = new ItemStack(org.bukkit.Material.ENCHANTED_BOOK);
			ItemMeta m2 = i1.getItemMeta();
			ArrayList<String> lore2 = new ArrayList<String>();

			lore2.add("Digs a region of 5x5");

			m2.setLore(lore2);
			m2.setDisplayName(ChatColor.BOLD + "Trench II");

			i2.setItemMeta(m2);
			trench.setItem(4, i2);

			// level 3

			ItemStack i3 = new ItemStack(org.bukkit.Material.ENCHANTED_BOOK);
			ItemMeta m3 = i1.getItemMeta();
			ArrayList<String> lore3 = new ArrayList<String>();

			lore3.add("Digs a region of 10x10");

			m3.setLore(lore3);
			m3.setDisplayName(ChatColor.BOLD + "Trench III");

			i3.setItemMeta(m3);
			trench.setItem(6, i3);

			p.openInventory(trench);

		}

		return false;

	}

	@EventHandler
	public void inventoryClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		ItemStack item = p.getItemInHand();
		if (e.getView().getTopInventory().getTitle().equals("Select a Trench level")) {
			if (e.getCurrentItem().getType() == Material.ENCHANTED_BOOK) {
				e.setCancelled(true);
				ItemMeta meta = item.getItemMeta();
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.GRAY + ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
				meta.setLore(lore);
				p.getItemInHand().setItemMeta(meta);

			}
		}
	}
}
