package me.renshoutepen.trench;

public enum BreakDirection {

	NORTH, EAST, SOUTH, WEST, UP, FLOOR;

}
