package me.renshoutepen.trench;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public static Main instance;

	public void onEnable() {
		instance = this;
		getServer().getPluginManager().registerEvents(new Trench(), this);
		getServer().getPluginManager().registerEvents(new GUICommand(), this);
		getServer().getPluginManager().registerEvents(new EnchantTable(), this);

		// this.getCommand("trench").setExecutor(new GUICommand());

	}

	public void onDisable() {
		instance = null;
	}
}
