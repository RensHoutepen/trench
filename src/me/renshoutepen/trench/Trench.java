package me.renshoutepen.trench;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class Trench implements Listener {

	@EventHandler
	public void getblock(BlockBreakEvent e) {
		if (isTool(e.getPlayer().getItemInHand().getType())) {
			if (e.getPlayer().getItemInHand().getItemMeta().hasLore()) {
				if (e.getPlayer().getItemInHand().getItemMeta().getLore().get(0).contains("Trench")) {
					final int range = intToRange(convertInt(ChatColor.stripColor(
							e.getPlayer().getItemInHand().getItemMeta().getLore().get(0).replaceAll("Trench ", ""))));
					final Player p = e.getPlayer();
					final Location loc = e.getBlock().getLocation();
					double rotation = (p.getLocation().getYaw() - 90) % 360;
					if (rotation < 0) {
						rotation += 360.0;
					}
					BreakDirection breakDirection = BreakDirection.NORTH;
					double pitch = p.getLocation().getPitch();
					if (rotation >= 45 && rotation < 135) {
						breakDirection = BreakDirection.NORTH;
					}
					if (rotation >= 135 && rotation < 225) {
						breakDirection = BreakDirection.EAST;
					}
					if (rotation >= 225 && rotation < 315) {
						breakDirection = BreakDirection.SOUTH;
					}
					if ((rotation >= 315 && rotation <= 360) || (rotation >= 0 && rotation < 45)) {
						breakDirection = BreakDirection.WEST;
					}
					if (pitch >= -90 && pitch <= -45) {
						breakDirection = BreakDirection.FLOOR;
					}
					if (pitch <= 90 && pitch >= 45) {
						breakDirection = BreakDirection.FLOOR;
					}
					breakBlocks(e.getBlock().getLocation(), breakDirection, range);
				}
			}
		}
	}

	public int intToRange(int ds) {
		if (ds == 1) {
			return 3;
		} else if (ds == 2) {
			return 5;
		} else if (ds == 3) {
			return 10;
		}
		return 0;
	}

	public int convertInt(String roman) {
		return roman.length();
	}

	public boolean isTool(Material material) {
		return material.toString().contains("AXE");
	}

	private void breakBlocks(Location loc, BreakDirection breakDirection, Integer range) {
		int r = calcRange(range);
		if (breakDirection.equals(BreakDirection.EAST) || breakDirection.equals(BreakDirection.WEST)) {
			for (int y = loc.getBlockY() - r; y <= loc.getBlockY() + r; y++) {
				for (int z = loc.getBlockZ() - r; z <= loc.getBlockZ() + r; z++) {
					Block b = loc.getWorld().getBlockAt(loc.getBlockX(), y, z);
					new BukkitRunnable() {

						@Override
						public void run() {
							b.breakNaturally();

						}
					}.runTaskLater(Main.instance, calcDelay(loc, b.getLocation()));

				}
			}
		} else if (breakDirection.equals(BreakDirection.SOUTH) || breakDirection.equals(BreakDirection.NORTH)) {
			for (int x = loc.getBlockX() - r; x <= loc.getBlockX() + r; x++) {
				for (int y = loc.getBlockY() - r; y <= loc.getBlockY() + r; y++) {
					Block b = loc.getWorld().getBlockAt(x, y, loc.getBlockZ());
					new BukkitRunnable() {

						@Override
						public void run() {
							b.breakNaturally();

						}
					}.runTaskLater(Main.instance, calcDelay(loc, b.getLocation()));
				}
			}
		} else if (breakDirection.equals(BreakDirection.FLOOR)) {

			for (int x = loc.getBlockX() - r; x <= loc.getBlockX() + r; x++) {
				for (int z = loc.getBlockZ() - r; z <= loc.getBlockZ() + r; z++) {
					Block b = loc.getWorld().getBlockAt(x, loc.getBlockY(), z);
					new BukkitRunnable() {

						@Override
						public void run() {
							b.breakNaturally();

						}
					}.runTaskLater(Main.instance, calcDelay(loc, b.getLocation()));
				}
			}

		}
	}

	public int calcRange(Integer range) {
		range--;
		range = range / 2;
		return range;
	}

	private int calcDelay(Location orig, Location newBlock) {
		float x = (float) (orig.getX() - newBlock.getX());
		float y = (float) (orig.getX() - newBlock.getX());
		float z = (float) (orig.getZ() - newBlock.getZ());
		if (x < 0) {
			x = -x;
		}
		if (y < 0) {
			y = -y;
		}
		if (z < 0) {
			z = -z;
		}
		int delay = Math.round(x + y + z);
		return delay;

	}
}
